# Table of contents

* [Home Assistant](README.md)

## Configuration

* [Getting Started](configuration/getting-started.md)

## Networking

* [NginxProxyManager](networking/nginxproxymanager.md)

## Integrations

* [Addons](integrations/addons.md)
* [HACS](integrations/hacs.md)
* [HA-Integrations](integrations/ha-integrations.md)

## Sensors

* [Templates](sensors/templates.md)

## Lovelace

* [Yaml](lovelace/yaml.md)
